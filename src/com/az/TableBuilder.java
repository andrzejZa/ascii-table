package com.az;


import java.util.ArrayList;
import java.util.List;

public class TableBuilder {
    private AsciiTable t;
    private List<String[]> table;

    public TableBuilder() {
        t = new AsciiTable();
        table = new ArrayList<>();
    }

    public void AddHeader(String[] header)
    {
        table.add(header);
    }



    public void AddRow(String[] data){
        table.add(data);
    }


    public void Print(){
        t.setBorderLessTable(true);
       // t.setColumnAlignment(1,AsciiTable.ALIGN_LEFT);
       // t.setColumnAlignment(2,AsciiTable.ALIGN_LEFT);
        t.adjustColumnSize(table);
        t.render(table);
    }
}
